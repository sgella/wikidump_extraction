import sys
import codecs
import re
import csv

#python process_file.py wiki_language_table.txt wiki_ISO_369_language_codes.csv

lines = codecs.open(sys.argv[1], 'r').readlines()
writer = csv.writer(open(sys.argv[2],'w'))
language_dict = {}

writer.writerow(['language', 'family', 'alternatives', 'code_alternatives', 'code', '3_code', 'other_code', 'extra_1', 'extra_2', 'macro_details'])

for line in lines:
	if line[:2] not in ['|-', '{|', '|}']:
		#spl = map(lambda s: s.decode('utf-8'), line.split('||'))
		spl = line.split('||')
		for i in range(1, len(spl)):
			spl[i] = [word for word in re.sub('[()\[\]\"\']','',spl[i]).replace(';',',').strip().split('|') if word!='']
			spl[i] = ",".join(spl[i])
			#print spl[i]
		language_rows = spl[2].split(',')
		if spl[3][:4] == "lang":
			other_rows = spl[3].split(',')[1:]
			for item in other_rows:
				if '<' not in item:
					language_rows.append(item)
			print language_rows
		for i in range(len(language_rows)):
			key = language_rows[i].replace('language', '').strip().lower()
			if not language_dict.has_key(key) and key!='':
				language_dict[key] = spl[1:]	
			#spl[0] = language_rows[i]
for key in language_dict:
	spl = [key]
	spl.extend(language_dict[key])
	writer.writerow(spl)
		
