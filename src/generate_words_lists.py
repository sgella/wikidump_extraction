#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# =============================================================================
#  Version: 1.0 (Nov 26, 2013)
#  Author: Spandana Gella (t-spgell@microsoft.com, spandanagella@gmail.com), Microsoft Research, India
#          
#
# =============================================================================
#  Copyright (c) 2013. Spandana Gella (spandanagella@gmail.com).
# =============================================================================
#
#  This software uses external software WikiExtractor.py which is part 
#  of Tanl from Guiseppi Atardi (attardi@di.unipi.it) and ISO 639 table listed on Wikipedia
#
#  This is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License, version 1,
#  as published by the Free Software Foundation.
#
#  This is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =============================================================================


"""Wordlist Generator from raw corpora:
Extracts the unique words from Wikipedia language specific database dump clean text and generates frequency based list.

Usage:
  generate_words_lists.py [options]

Options:
  -d, --data            : path to data directory, by default it is set to data
  -l, --language        : language for which wiki dump processing should be done, default set to telugu
                          current)
  -s, --sections        : preserve sections
  -h, --help            : display this help and exit
"""

import sys
import os.path
import codecs
import operator
import csv

def main():

        script_name = os.path.basename(sys.argv[0])

        from optparse import OptionParser
        opts = OptionParser(add_help_option=False)
        opts.add_option("-d", "--data", dest="data", default="../data")
        opts.add_option("-x", "--language", dest="language", default="telugu")
        opts.add_option('-h', '--help', dest='help', default=False)

	options, args = opts.parse_args()

        if options.help:
                show_help()

	language_dict = {}
        reader = csv.reader(codecs.open(os.path.join(options.data, "wiki_ISO_369_language_codes.csv"), 'r'))
        reader.next()
        for row in reader:
                language_dict[row[0]] = row[4]

	try:
                #print language_dict.keys()
                if not language_dict.has_key(options.language):
                        raise ValueError()
		else:
			if not os.path.exists(os.path.join(options.data, "wordlists")):
				os.makedirs(os.path.join(options.data, "wordlists"))
			code = language_dict[options.language]
			src_dir = "/".join(os.path.abspath(sys.argv[0]).split("/")[:-1])
			if not os.path.exists(os.path.join(options.data,'latest-pages-articles.'+code+".refined")):
				#call the WikiLanguageExtractor code to create the corpus
				wiki_extractor_path = os.path.join(src_dir, 'WikiLanguageExtractor.py')
				wiki_extractor_command = 'python %s %s' % (wiki_extractor_path, " ".join(sys.argv[1:]))
				print wiki_extractor_command
				os.system(wiki_extractor_command)
			word_freq = {}
			for line in codecs.open(os.path.join(options.data,'latest-pages-articles.'+code+".refined"), 'r'):
				words = line.split()
				for word in words:
					#word = word.decode('utf-8')
					if not word_freq.has_key(word):
						word_freq[word] = 0 
					word_freq[word] +=1
			f1 = codecs.open(os.path.join(options.data, "wordlists", "word_freq_list."+code), 'w')
			#print word_freq.keys()[:5]
			for key, value in sorted(word_freq.iteritems(), key=operator.itemgetter(1), reverse=True):
				f1.write(key+"\t"+str(word_freq[key])+"\n")
			f1.close()
					
	except ValueError:
        	print >> sys.stderr, \
                'Entered %s is not valid' % (options.language)
	except Exception, e:
		print "<p>Error: %s</p>" % str(e) 


if __name__ == '__main__':
	main()
	
