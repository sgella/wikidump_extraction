#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# =============================================================================
#  Version: 1.0 (Nov 26, 2013)
#  Author: Spandana Gella (t-spgell@microsoft.com, spandanagella@gmail.com), Microsoft Research, India
#          
#
# =============================================================================
#  Copyright (c) 2013. Spandana Gella (spandanagella@gmail.com).
# =============================================================================
#
#  This software uses external software WikiExtractor.py which is part 
#  of Tanl from Guiseppi Atardi (attardi@di.unipi.it) and ISO 639 table listed on Wikipedia
#
#  This is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License, version 1,
#  as published by the Free Software Foundation.
#
#  This is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =============================================================================

"""Wikipedia Language Based Extractor:
Extracts and cleans text from Wikipedia language specific database dump and stores output in a
number of files of similar size in a given directory or dumps altogether into a single file.

Each file contains several documents which are appeneded next to each other followed by their titles:

Usage:
  WikiLanguageExtractor.py [options]

Options:
  -d, --data		: path to data directory, by default it is set to data
  -c, --compress        : compress output files using bzip
  -b, --bytes= n[KM]    : put specified bytes per output file (default 10mb)
  -x, --language	: language for which wiki dump processing should be done, default set to telugu
  -f, --full		: if the whole dump needed to be in single file, by default set True
  -B, --base= URL       : base URL for the Wikipedia pages
  -l, --link            : preserve links
  -n NS, --ns NS        : accepted namespaces (separated by commas)
  -o, --output= dir     : place output files in specified directory (default
                          current)
  -s, --sections        : preserve sections
  -h, --help            : display this help and exit
"""



import sys
import gc
import bz2
import os.path
import csv
import codecs
import WikiExtractor
import clean_dump
import shutil


#def get_language_dump(language, options):


def show_usage(script_name):
    print >> sys.stderr, 'Usage: %s [options]' % script_name

def show_help():
    print >> sys.stdout, __doc__,
    #WikiExtractor.show_help()


def main():

	script_name = os.path.basename(sys.argv[0])

	from optparse import OptionParser
        opts = OptionParser(add_help_option=False)
        opts.add_option("-d", "--data", dest="data", default="../data")
        opts.add_option("-x", "--language", dest="language", default="telugu")
        opts.add_option("-f", "--full", dest="full", default=True)
	opts.add_option('-h', '--help', dest='help', default=False)
	opts.add_option('-c', '--clean', dest='clean', default = True)
	opts.add_option('-r', '--refine', dest='refine', default = True)


        options, args = opts.parse_args()

	if options.help:
		show_help()
	
	language_dict = {}
	reader = csv.reader(codecs.open(os.path.join(options.data, "wiki_ISO_369_language_codes.csv"), 'r'))
	reader.next()
	for row in reader:
		language_dict[row[0]] = row[4]
	
	try:
		#print language_dict.keys()
		if not language_dict.has_key(options.language):
			raise ValueError()
		else:
			#call WikiExtractor code
			code = language_dict[options.language]
			wget_command = 'wget http://download.wikimedia.org/%swiki/latest/%swiki-latest-pages-articles.xml.bz2' % (code, code)
			src_dir = "/".join(os.path.abspath(sys.argv[0]).split("/")[:-1])
			wiki_extractor_path = os.path.join(src_dir, 'WikiExtractor.py')
			extract_dir = '%s_extracted' % (os.path.join(options.data, code))
			create_dir_command = 'mkdir %s' %(extract_dir)
			extract_command = 'bzip2 -dc %swiki-latest-pages-articles.xml.bz2 |  %s -o %s ' %(os.path.join(src_dir,code), wiki_extractor_path, extract_dir)
			if not os.path.exists(os.path.join(src_dir,code+'wiki-latest-pages-articles.xml.bz2')):
				print wget_command
				os.system(wget_command)
			if not os.path.exists(extract_dir):
				print create_dir_command
				os.system(create_dir_command)
			if not len(os.listdir(extract_dir)) >0:
				print extract_command
				os.system(extract_command)
			
			if options.full and not os.path.exists(os.path.join(options.data, 'latest-pages-articles.'+code)):
				extract_dump_command = 'find %s -name \'*bz2\' -exec bunzip2 -c {} \; > %s' %(extract_dir, os.path.join(options.data, 'latest-pages-articles.'+code))
				print extract_dump_command
				os.system(extract_dump_command)
			if options.refine and not os.path.exists(os.path.join(options.data, 'latest-pages-articles.'+code+".refined")):
				if code == "en":
					clean_dump.clean(os.path.join(options.data, 'latest-pages-articles.'+code), os.path.join(options.data, 'latest-pages-articles.'+code+".refined"), False)
				else:
					clean_dump.clean(os.path.join(options.data, 'latest-pages-articles.'+code), os.path.join(options.data, 'latest-pages-articles.'+code+".refined"), True)
			
			if options.clean:
				if os.path.exists(os.path.join(src_dir,code+"wiki-latest-pages-articles.xml.bz2")):
					os.remove(os.path.join(src_dir,code+"wiki-latest-pages-articles.xml.bz2"))
				shutil.rmtree(extract_dir)	
				if options.full:
					os.remove(os.path.join(options.data, 'latest-pages-articles.'+code))	
			
        except ValueError:
        	print >> sys.stderr, \
                'Entered %s is not valid' % (options.language)		
	
if __name__ == '__main__':
	main()
	
