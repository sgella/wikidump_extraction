import os
import codecs
import sys
from nltk import word_tokenize, wordpunct_tokenize
import re
def clean(inp_file, out_file, unicodeFlag):
	f1 = codecs.open(out_file, 'w')
	for line in codecs.open(inp_file):
		refined_line = []
		line = re.sub('[=\'-]', ' ', line)
		words = word_tokenize(line)
		if unicodeFlag:
			for word in words:
				xword = word.decode('utf-8')
				if word!=xword:
					refined_line.append(word)
			refined_line = " ".join(refined_line).strip()
		else:
			refined_line = " ".join(words).strip()	
		if refined_line!="" and len(refined_line)>5:
			f1.write(refined_line+"\n")
	f1.close()
			

if __name__ == '__main__':
	clean("../data/latest-pages-articles.te", "../data/latest-pages-articles.te.refined", True)
